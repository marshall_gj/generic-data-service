package com.example.web.api;

import com.example.service.IStaticDataService;
import com.example.web.api.UploadStaticDataResource;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UploadStaticDataResource.class)
class UploadStaticDataResourceTest {

    @MockBean
    private IStaticDataService staticDataService;
    private final MockMvc mockMvc;

    @Autowired
    public UploadStaticDataResourceTest(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Test
    public void testReturnOK() throws Exception {
        String requestBody = "Some test request body";
        this.mockMvc.perform(post("/static/upload/").content(requestBody)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("OK")));
        Mockito.verify(staticDataService).processStaticData(requestBody);
    }

    @Test
    public void testStatusBadRequest() throws Exception {
        //missing any content and therefore @RequestBody is null
        this.mockMvc.perform(post("/static/upload/")).andDo(print()).andExpect(status().isBadRequest());
    }
}