package com.example.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties("app")
public class AppProperties {

    //rabbit mq config
    private String rabbitMQQueueName;
    private boolean durableQueue;
    //kafka config
    private String kafkaBootstrapAddress;
    private String kafkaTopicName;
    private int kafkaPartitions;
    private short kafkaReplicationNumber;

    public String getRabbitMQQueueName() {
        return rabbitMQQueueName;
    }

    public void setRabbitMQQueueName(String rabbitMQQueueName) {
        this.rabbitMQQueueName = rabbitMQQueueName;
    }

    public boolean isDurableQueue() {
        return durableQueue;
    }

    public void setDurableQueue(boolean durableQueue) {
        this.durableQueue = durableQueue;
    }

    public String getKafkaTopicName() {
        return kafkaTopicName;
    }

    public void setKafkaTopicName(String kafkaTopicName) {
        this.kafkaTopicName = kafkaTopicName;
    }

    public int getKafkaPartitions() {
        return kafkaPartitions;
    }

    public void setKafkaPartitions(int kafkaPartitions) {
        this.kafkaPartitions = kafkaPartitions;
    }

    public short getKafkaReplicationNumber() {
        return kafkaReplicationNumber;
    }

    public void setKafkaReplicationNumber(short kafkaReplicationNumber) {
        this.kafkaReplicationNumber = kafkaReplicationNumber;
    }

    public String getKafkaBootstrapAddress() {
        return kafkaBootstrapAddress;
    }

    public void setKafkaBootstrapAddress(String kafkaBootstrapAddress) {
        this.kafkaBootstrapAddress = kafkaBootstrapAddress;
    }
}