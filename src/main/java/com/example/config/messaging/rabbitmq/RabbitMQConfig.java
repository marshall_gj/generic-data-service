package com.example.config.messaging.rabbitmq;

import com.example.config.properties.AppProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {

    private final AppProperties appProperties;

    public RabbitMQConfig(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    @Bean
    public Queue staticDataQueue() {
        return new Queue(appProperties.getRabbitMQQueueName(), appProperties.isDurableQueue());
    }
}