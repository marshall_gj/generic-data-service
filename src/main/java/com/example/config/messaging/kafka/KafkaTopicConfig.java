package com.example.config.messaging.kafka;

import com.example.config.properties.AppProperties;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
@ConditionalOnProperty("${app.kafka.topic.config.enabled}")
public class KafkaTopicConfig {

    private final AppProperties appProperties;

    public KafkaTopicConfig(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    @Bean
    public NewTopic staticDataTopic() {
        return new NewTopic(appProperties.getKafkaTopicName(),
                appProperties.getKafkaPartitions(), appProperties.getKafkaReplicationNumber());
    }
}