package com.example.web.api;

import com.example.service.IStaticDataService;
import com.example.web.constants.WebConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/static/")
public class UploadStaticDataResource {

    private final Logger log = LoggerFactory.getLogger(UploadStaticDataResource.class);
    private final IStaticDataService staticDataService;

    public UploadStaticDataResource(IStaticDataService staticDataService) {
        this.staticDataService = staticDataService;
    }

    @PostMapping(path = "/upload/")
    public ResponseEntity<String> upload(@RequestBody String requestBody) {
        //we could use e.g. a custom request header to determine the source
        //and therefore the data parsing/manipulation that is required
        log.info("Incoming request body: " + requestBody);
        staticDataService.processStaticData(requestBody);
        return ResponseEntity.ok(WebConstants.OK_RESPONSE);
    }
}