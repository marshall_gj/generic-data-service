package com.example.service;

import com.example.config.properties.AppProperties;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaMessagingServiceImpl implements IMessagingService {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final AppProperties appProperties;

    public KafkaMessagingServiceImpl(KafkaTemplate<String, String> kafkaTemplate, AppProperties appProperties) {
        this.kafkaTemplate = kafkaTemplate;
        this.appProperties = appProperties;
    }

    @Override
    public void sendMessage(String message) {
        kafkaTemplate.send(appProperties.getKafkaTopicName(), message);
    }
}