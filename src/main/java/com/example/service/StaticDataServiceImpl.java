package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class StaticDataServiceImpl implements IStaticDataService {

    private final IMessagingService rabbitMQService;
    private final IMessagingService kafkaMessagingService;

    @Autowired
    public StaticDataServiceImpl(@Qualifier("rabbitMQMessagingServiceImpl") IMessagingService rabbitMQService,
                                 @Qualifier("kafkaMessagingServiceImpl") IMessagingService kafkaMessagingService) {
        this.rabbitMQService = rabbitMQService;
        this.kafkaMessagingService = kafkaMessagingService;
    }

    @Override
    public void processStaticData(String staticData) {

        //TODO: perform parsing of data (g.m.)

        //send the incoming blob to the rabbit queue
        rabbitMQService.sendMessage(staticData);
        //send the incoming blob to the kafka topic
        kafkaMessagingService.sendMessage(staticData);
    }
}