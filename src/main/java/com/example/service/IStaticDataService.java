package com.example.service;

public interface IStaticDataService {
    void processStaticData(String staticData);
}