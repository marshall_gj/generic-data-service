package com.example.service;

public interface IMessagingService {
    void sendMessage(String message);
}