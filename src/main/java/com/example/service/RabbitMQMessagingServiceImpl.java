package com.example.service;

import com.example.config.properties.AppProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQMessagingServiceImpl implements IMessagingService {

    private final RabbitTemplate rabbitTemplate;
    private final AppProperties appProperties;

    public RabbitMQMessagingServiceImpl(RabbitTemplate rabbitTemplate, AppProperties appProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.appProperties = appProperties;
    }

    @Override
    public void sendMessage(String message) {
        rabbitTemplate.convertAndSend(appProperties.getRabbitMQQueueName(), message);
    }
}
